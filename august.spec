%define pkgname august
%define ver 0.62b.wrp
%define rel 2

%define pkg %{pkgname}-%{ver}-%{rel}

Summary: A TCL/TK html editor.
Name: %{pkgname}
Version: %{ver}
Release: %{rel}
Copyright: GPL
Group: Applications/Internet
Source: http://www.lls.se/~johanb/august/august0.62b.wrp.tar.gz
URL: http://www.lls.se/~johanb/august/
Patch: august-0.patch
Distribution: freeduc
Vendor: OFSET
Packager: Hilaire Fernandes <hilaire@ofset.org>
BuildRoot: %{_tmppath}/%{pkgname}-%{version}
Provides: %{pkgname}
#xsRequires: 

%description 
August is a free html editor for the UNIX platform. It's a non-wysiwyg
editor - like Aswedit or Hotdog. It's written with the Tcl/TK
scripting language and graphical toolkit. However this version doesn't
require TCL/TK.

%prep
%setup -q -n %{pkgname}%{ver}
%patch -p1

%build

%install 
make DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/usr/bin/august
/usr/share/gnome/apps/Freeduc/Other/august.desktop
%doc install.txt keyname.tcl license.txt readme.txt specchars.txt

%changelog
* Fri Nov 17 2000 Hilaire Fernandes <hilaire@ofset.org>
- Create .spec file for august 0.62b.wrp
